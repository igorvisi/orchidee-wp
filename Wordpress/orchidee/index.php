<?php get_header(); ?>

<header id="navigation" class="navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigation</span>
				<i class="fa fa-bars fa-2x"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
				<h1 id="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" width="50" alt="">
					<span class="logo-text">Orchidée Event</span>
				</h1>
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right" role="navigation">
			<ul id="nav" class="nav navbar-nav  ">
				<li><a href="//orchideevent.net" class="external">Accueil</a></li>
				<li class="dropdown">
					<a class="external" href="" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos services<span class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown">
						<li><a class="external" href="/design">Wedding design</a></li>
						<li><a class="external" href="/planning">Wedding planning</a></li>
						<li><a class="external" href="/location">Location des matériels</a></li>

					</ul>

				</li>
				<li><a href="/media" class="external">Espace media</a></li>
				<li><a href="/news" class="external current">Actualités</a></li>
				<li><a href="/contact" class="external">Nous contacter</a></li>
			</ul>
		</nav>
		<!-- /main nav -->

	</div>
</header>

<section id="recents" class="features padding-top-50">
	<div class="container-fluid">
		<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
			<h2>Actualités</h2>
			<div class="devider"><i class="far fa-heart"></i></i></div>
		</div>

		<div class="row">

			<div class="col-sm-8">
				<?php
				if (have_posts()) : while (have_posts()) : the_post();

						get_template_part('template-parts/content', get_post_type());

					endwhile;

					the_posts_pagination(array(
						'prev_text' => __('Précédent'),
						'next_text' => __('Suivant'),
					));

				endif;
				?>
			</div>
			<div class="col-sm-4">
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>