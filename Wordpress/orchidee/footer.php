<!--
						        End About
						        ==================================== -->

<footer id="footer" class="footer">
	<div class="container" style="padding-top: 30px;">
		<div class="row">

			<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
				<div class="footer-single">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="100" alt="">
					<p>Orchidée Event,"Le Temple des events", est un centre moderne et premium d'évènements composée de créateurs de rêves
						inimaginables
						appelés à devenir réalité.</p>
				</div>
			</div>


			<div class="col-md-3 col-sm-6 col-xs-12 pt-3 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
				<div class="footer-single">
					<h6>Sur les réseaux sociaux</h6>
					<ul>
						<li><a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i> Facebook</a></li>
						<li><a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i> Instagram</a></li>
						<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"> Messenger</i></a></li>
						<li><a href="mailto:infos@orchideevent.com"><i class="fas fa-envelope"></i> infos@orchideevent.com</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
				<div class="footer-single">
					<h6>Nos services</h6>
					<ul>
						<li><a href="/design">Wedding design</a></li>
						<li><a href="/planning">Wedding planning</a></li>
						<li><a href="/location">Location des matériels</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
				<div class="footer-single">
					<h6>Plus d'informations</h6>
					<ul>
						<li><a href="/contact">Nous contacter</a></li>
						<li><a href="/actualites">Actualités</a></li>
						<li><a href="/media">Espaces média</a></li>
						<li><a href="/wp-admin">Connexion</a></li>
					</ul>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="copyright text-center">
					Copyright © 2019 <a href="<?php echo get_home_url(); ?>">Orchidée Event</a>
				</p>
			</div>
		</div>
	</div>
</footer>