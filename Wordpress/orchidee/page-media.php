<?php get_header(); ?>

<header id="navigation" class="navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigation</span>
				<i class="fa fa-bars fa-2x"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
				<h1 id="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" width="50" alt="">
					<span class="logo-text">Orchidée Event</span>
				</h1>
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right" role="navigation">
			<ul id="nav" class="nav navbar-nav  ">
				<li><a href="//orchideevent.net" class="external">Accueil</a></li>
				<li class="dropdown">
					<a class="external" href="" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos services<span class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown">
						<li><a class="external" href="/design">Wedding design</a></li>
						<li><a class="external" href="/planning">Wedding planning</a></li>
						<li><a class="external" href="/location">Location des matériels</a></li>

					</ul>

				</li>
				<li><a href="/media" class="external current">Espace media</a></li>
				<li><a href="/actualites" class="external">Actualités</a></li>
				<li><a href="/contact" class="external">Nous contacter</a></li>
			</ul>
		</nav>
		<!-- /main nav -->

	</div>
</header>
<!-- End Fixed Navigation -->


<!--Service location -->

<section id="works" class="works padding-top-50 clearfix">
	<div class="container">
		<div class="row">

			<div class="sec-title text-center">
				<h2>Events</h2>
				<div class="devider"><i class="far fa-heart"></i></div>
			</div>

			<div class="sec-sub-title text-center">
				<p>Des événements à votre guise !&nbsp;Soirées, cocktails, inaugurations… Nous vous accompagnons dans
					tous vos
					projets, du plus simple au plus ambitieux. Orchidée Event s’adapte à vos dispositions.</p>
			</div>

		</div>
	</div>



	<?php
	global $post;
	global $wp_query;
	$currentPageCatId = get_cat_ID("pagename");
	$NewsCatPostsArray = query_posts('cat=' . $currentPageCatId);

	$current_news_postId = $NewsCatPostsArray[$i]->ID;
	$current_news_postUrl = get_permalink($current_news_postId);
	$query_images_args = array('post_type' => 'attachment', 'post_mime_type' => 'image', 'post_status' => 'inherit', 'posts_per_page' => -1, 'post_parent' => $current_news_postId);

	$query_images = new WP_Query($query_images_args);
	$images = array();
	?>


	<?php foreach ($query_images->posts as $image) : ?>

		<figure class="mix work-item branding" style="display: inline-block;">
			<img src="<?php echo $image->guid; ?>" alt="">
			<figcaption class="overlay">
				<a class="fancybox" rel="works" title="<?php echo "Ok"; ?>" href="<?php echo $image->guid; ?>"><i class="fa fa-eye fa-lg"></i></a>
			</figcaption>
		</figure>
	<?php endforeach; ?>




	<div class="project-wrapper" id="MixItUpAC9E8F">



	</div>


</section>

<!-- End About ==================================== -->

<?php get_footer(); ?>