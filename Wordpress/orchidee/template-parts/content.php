<?php

/**
 * Template part for displaying posts
 */

?>
<div class="row" style="margin-bottom: 20px;">

	<div class="col-sm-5">
		<div class="thumbnail">
			<a href="<?php echo get_permalink(); ?>">
				<img src="<?php the_post_thumbnail_url(); ?>" alt="">
			</a>
		</div>
	</div>

	<div class="col-sm-7">
		<div class="caption">
			<h3>
				<a href="<?php echo get_permalink(); ?>" class="text-primary">
					<?php the_title(); ?>
				</a>
			</h3>
			<p>
				<?php the_excerpt(); ?>
			</p>
		</div>
	</div>
</div>