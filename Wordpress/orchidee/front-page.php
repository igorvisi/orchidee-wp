<?php get_header(); ?>
<!--
        Fixed Navigation
				==================================== -->
<header id="navigation" class="navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigation</span>
				<i class="fa fa-bars fa-2x"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href=" <?php echo get_home_url(); ?> ">
				<h1 id="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" width="50" alt="">
					<span class="logo-text">Orchidée Event</span>
				</h1>
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right" role="navigation">
			<ul id="nav" class="nav navbar-nav  ">
				<li><a href="//orchideevent.net" class="external current">Accueil</a></li>
				<li class="dropdown">
					<a class="external" href="" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos services<span class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown">
						<li><a class="external" href="/design">Wedding design</a></li>
						<li><a class="external" href="/planning">Wedding planning</a></li>
						<li><a class="external" href="/location">Location des matériels</a></li>

					</ul>

				</li>
				<li><a href="/media" class="external">Espace media</a></li>
				<li><a href="/actualites" class="external">Actualités</a></li>
				<li><a href="/contact" class="external">Nous contacter</a></li>
			</ul>
		</nav>
		<!-- /main nav -->
		<!-- /main nav -->

	</div>
</header>

<!--
        End Fixed Navigation
        ==================================== -->
<?php

$qui_sommes_nous = get_field("qui_sommes_nous");

$slider1 = get_field("slide1");
$slider2 = get_field("slide2");
$slider3 = get_field("slide3");
$slider4 = get_field("slide4");

$service1 = get_field("service1");
$service2 = get_field("service2");
$service3 = get_field("service3");



?>

<!-- =================== Home Slider ==================================== -->

<section id="slider">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

		<!-- Indicators bullet -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			<li data-target="#carousel-example-generic" data-slide-to="3"></li>
		</ol>
		<!-- End Indicators bullet -->

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">



			<!-- single slide -->
			<div class="item active" style="background-image: url(<?php echo $slider1["image"] ?>),linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5));background-blend-mode: overlay;">
				<div class="carousel-caption">
					<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated"> <span><?php echo $slider1["titre"] ?> </span> </h2>
					<h3 data-wow-duration="500ms" class="wow slideInLeft animated"> <?php echo $slider1["description"] ?></h3>
					<p data-wow-duration="200ms" class="wow slideInRight animated"><?php echo $slider1["accroche"] ?></p>
					<ul class="social-links text-center">
						<li><a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i></a></li>
						<li><a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i></a></li>
						<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"></i></a></li>

					</ul>
				</div>
			</div>
			<!-- end single slide -->


			<!-- single slide -->
			<div class="item" style="background-image: url(<?php echo $slider2["image"] ?>),linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5));background-blend-mode: overlay;">
				<div class="carousel-caption">
					<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span><?php echo $slider2["titre"] ?></span></h2>
					<h3 data-wow-duration="500ms" class="wow slideInLeft animated"> <?php echo $slider2["description"] ?> </h3>
					<p data-wow-duration="200ms" class="wow slideInRight animated"><?php echo $slider2["accroche"] ?></p>
					<ul class="social-links text-center">
						<li><a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i></a></li>
						<li><a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i></a></li>
						<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"></i></a></li>
					</ul>

				</div>
			</div>
			<!-- end single slide -->

			<!-- single slide -->
			<div class="item" style="background-image: url(<?php echo $slider3["image"] ?>),linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5));background-blend-mode: overlay;">
				<div class="carousel-caption">
					<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span><?php echo $slider3["titre"] ?></span></h2>
					<h3 data-wow-duration="500ms" class="wow slideInLeft animated"> <?php echo $slider3["description"] ?> </h3>
					<p data-wow-duration="200ms" class="wow slideInRight animated"><?php echo $slider3["accroche"] ?></p>
					<ul class="social-links text-center">
						<li>
							<a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i>
							</a>
						</li>
						<li>
							<a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i></a>
						</li>
						<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"></i></a>
						</li>
					</ul>
				</div>
			</div>

			<!-- single slide -->
			<div class="item" style="background-image: url(<?php echo $slider4["image"] ?>),linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5));background-blend-mode: overlay;">
				<div class="carousel-caption">
					<h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated"><span><?php echo $slider4["titre"] ?></span></h2>
					<h3 data-wow-duration="500ms" class="wow slideInLeft animated"> <?php echo $slider4["description"] ?> </h3>
					<p data-wow-duration="200ms" class="wow slideInRight animated"><?php echo $slider4["accroche"] ?></p>

					<ul class="social-links text-center">
						<li><a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i></a></li>
						<li><a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i></a></li>
						<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"></i></a></li>

					</ul>

				</div>
			</div>

		</div>
		<!-- End Wrapper for slides -->

	</div>
</section>

<!--
        End Home SliderEnd
        ==================================== -->

<!--
	        About
	        ==================================== -->

<section id="about" class="features">
	<div class="container">
		<div class="row">

			<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>Qui sommes-nous ?</h2>
				<div class="devider"><i class="far fa-heart"></i></i></div>
			</div>

			<!-- Description -->
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="500ms">
				<div class="service-item">

					<div class="service-desc">
						<h3 class="text-center"><?php echo $qui_sommes_nous["titre"] ?></h3>
						<p class="text-justify">
							<?php echo $qui_sommes_nous["part1"] ?>
						</p>
						<hr>
						<p class="text-justify">
							<?php echo $qui_sommes_nous["part2"] ?>

						</p>
					</div>
				</div>
			</div>
			<!-- end Description -->

			<!-- Logo or team pic -->
			<div class="col-md-6 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="500ms">
				<div class="service-item">


					<div class="service-desc">
						<img src="<?php echo $qui_sommes_nous["image"] ?>" class="img-responsive" alt="Logo de Orchidée Event" srcset="">
					</div>
				</div>
			</div>
			<!-- end logo or team pic -->
		</div>
	</div>
</section>

<section id="features" class="features">
	<div class="container">
		<div class="row">

			<div class="sec-title text-center mb50 wow bounceInDown animated animated" data-wow-duration="500ms" style="visibility: visible; animation-duration: 500ms; animation-name: bounceInDown;">
				<h2>Nos services</h2>
				<div class="devider"><i class="far fa-heart"></i></div>
			</div>

			<!-- service item -->
			<div class="col-md-4 wow fadeInLeft animated" data-wow-duration="500ms" style="visibility: visible; animation-duration: 500ms; animation-name: fadeInLeft;">
				<div class="service-item">
					<div class="service-icon">
						<i class="fas <?php echo $service1["icon"] ?> fa-2x"></i>
					</div>

					<div class="service-desc">
						<h3><?php echo $service1["titre"] ?></h3>
						<p class="text-justify">
							<?php echo $service1["description"] ?>
						</p>
					</div>
				</div>
			</div>
			<!-- end service item -->

			<!-- service item -->
			<div class="col-md-4 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="500ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 500ms; animation-name: fadeInUp;">
				<div class="service-item">
					<div class="service-icon">
						<i class="fas <?php echo $service2["icon"] ?> fa-2x"></i>
					</div>

					<div class="service-desc">
						<h3> <?php echo $service2["titre"] ?> </h3>
						<p> <?php echo $service2["description"] ?></p>
					</div>
				</div>
			</div>
			<!-- end service item -->

			<!-- service item -->
			<div class="col-md-4 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="900ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 900ms; animation-name: fadeInRight;">
				<div class="service-item">
					<div class="service-icon">
						<i class="fas <?php echo $service3["icon"] ?> fa-2x"></i>
					</div>

					<div class="service-desc">
						<h3> <?php echo $service3["titre"] ?> </h3>
						<p> <?php echo $service3["description"] ?> </p>
					</div>
				</div>
			</div>
			<!-- end service item -->

		</div>
	</div>
</section>
<!-- ========================== End About==================================== -->


<!-- ========================== Some stats ==================================== -->

<section id="stats" class="stats">
	<div class="parallax-overlay">
		<div class="container">
			<div class="row number-counters">

				<div class="sec-title text-center mb50 wow rubberBand animated" data-wow-duration="1000ms">
					<h2>Satisfaction des clients</h2>
					<div class="devider"><i class="far fa-heart"></i></i></div>
				</div>

				<!-- first count item -->
				<div class="col-md-4 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="counters-item">
						<i class="fas fa-briefcase fa-3x"></i>
						<p>plus de</p>
						<strong data-to="3200">0</strong>
						<!-- Set Your Number here. i,e. data-to="56" -->
						<p>Heures de travail</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
					<div class="counters-item">
						<i class="fa fa-users fa-3x"></i>
						<p>plus de</p>
						<strong data-to="120">0</strong>
						<!-- Set Your Number here. i,e. data-to="56" -->
						<p>Clients satisfaits</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
					<div class="counters-item">
						<i class="fa fa-rocket fa-3x"></i>
						<p>plus de</p>
						<strong data-to="50">0</strong>
						<!-- Set Your Number here. i,e. data-to="56" -->
						<p> Events accompagnés </p>
					</div>
				</div>
				<!-- end first count item -->

			</div>
		</div>
	</div>
</section>

<!--
        End Some stats
        ==================================== -->


<!--
						        Recently
						        ==================================== -->

<section id="recents" class="features">
	<div class="container">
		<div class="row">

			<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>Récentes organisations</h2>
				<div class="devider"><i class="far fa-heart"></i></i></div>
			</div>


			<?php
			// the query
			$the_query = new WP_Query(array(
				'posts_per_page' => 3,
			));
			?>

			<?php if ($the_query->have_posts()) : ?>
				<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>



					<div class=" col-md-4">
						<div class="thumbnail">
							<a href="<?php echo get_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="..."></a>
							<div class="caption">
								<h3> <a href="<?php echo get_permalink(); ?>" class="text-primary"> <?php the_title(); ?></a> </h3>
								<p> <?php the_excerpt(); ?>

								</p>
							</div>
						</div>
					</div>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else : ?>

				<h1><?php __(''); ?></h1>
			<?php endif; ?>


		</div>
	</div>
</section>



<?php get_footer(); ?>