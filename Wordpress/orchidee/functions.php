<?php


@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');

/*
 * Let WordPress manage the document title.
 */
add_theme_support('title-tag');
/*
 * Enable support for Post Thumbnails on posts and pages.
 */
add_theme_support("post-thumbnails");

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support('html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
));

/**
 * Register widget area.
 *
 */
function untheme_widgets_init()
{
	register_sidebar(array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'description'   => 'Add widgets',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'untheme_widgets_init');


function untheme_create_post_custom_post()
{
	register_post_type(
		'custom_post',
		array(
			'labels' => array(
				'name' => __('Custom Post', 'untheme'),
			),
			'public'       => true,
			'hierarchical' => true,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'custom-fields',
				'thumbnail',
			),
			'taxonomies'   => array(
				'post_tag',
				'category',
			)
		)
	);
}
add_action('init', 'untheme_create_post_custom_post'); // Add our work type


function load_stylesheets()
{
	wp_deregister_script('jquery');

	wp_register_style("font", get_template_directory_uri() . "/css/all.min.css", array(), 1, "all");
	wp_enqueue_style("font");

	wp_register_style("bootstrap", get_template_directory_uri() . "/css/bootstrap.min.css", array(), 1, "all");
	wp_enqueue_style("bootstrap");

	wp_register_style("fancybox", get_template_directory_uri() . "/css/jquery.fancybox.css", array(), 1, "all");
	wp_enqueue_style("fancybox");

	wp_register_style("animate", get_template_directory_uri() . "/css/animate.css", array(), 1, "all");
	wp_enqueue_style("animate");

	wp_register_style("app", get_template_directory_uri() . "/css/app.css", array(), 1, "all");
	wp_enqueue_style("app");

	wp_register_style("media", get_template_directory_uri() . "/css/media-queries.css", array(), 1, "all");
	wp_enqueue_style("media");
}

add_action('wp_enqueue_scripts', 'load_stylesheets');


function header_script()
{
	wp_enqueue_script("modernize", get_template_directory_uri() . "/js/modernizr-2.6.2.min.js", array(), true);
}

add_action('wp_head', 'header_script');



function load_script()
{

	wp_enqueue_script("jquery", get_template_directory_uri() . "/js/jquery-1.11.1.min.js", array(), true);

	wp_enqueue_script("bootstrap", get_template_directory_uri() . "/js/bootstrap.min.js", array(), true);


	wp_enqueue_script("fancy", get_template_directory_uri() . "/js/jquery.fancybox.pack.js", array(), true);

	wp_enqueue_script("mixitup", get_template_directory_uri() . "/js/jquery.mixitup.min.js", array(), true);

	wp_enqueue_script("paralax", get_template_directory_uri() . "/js/jquery.parallax-1.1.3.js", array(), true);

	wp_enqueue_script("count", get_template_directory_uri() . "/js/jquery-countTo.js", array(), true);

	wp_enqueue_script("appear", get_template_directory_uri() . "/js/jquery.appear.js", array(), true);

	wp_enqueue_script("easing", get_template_directory_uri() . "/js/jquery.easing.min.js", array(), true);


	wp_enqueue_script("wow", get_template_directory_uri() . "/js/wow.min.js", array(), true);


	wp_enqueue_script("font", get_template_directory_uri() . "/js/fontawesome.js", array(), true);
}


add_action('wp_enqueue_scripts', 'load_script');


function customJS()
{

	wp_enqueue_script("custom", get_template_directory_uri() . "/js/custom.js", array(), true);
}

add_action('wp_enqueue_scripts', 'customJS');



add_filter('show_admin_bar', '__return_false');


function wp_get_attachment($attachment_id)
{

	$attachment = get_post($attachment_id);
	return array(
		'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink($attachment->ID),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}
