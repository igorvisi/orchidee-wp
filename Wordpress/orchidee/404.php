<?php get_header(); ?>

<?php

$part1 = get_field("part1");
$part2 = get_field("part2");
$part3 = get_field("part3");

?>

<header id="navigation" class="navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigation</span>
				<i class="fa fa-bars fa-2x"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
				<h1 id="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" width="50" alt="">
					<span class="logo-text">Orchidée Event</span>
				</h1>
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right" role="navigation">
			<ul id="nav" class="nav navbar-nav  ">
				<li><a href="//orchideevent.net" class="external">Accueil</a></li>
				<li class="dropdown">
					<a class="external " href="" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos services<span class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown">
						<li><a class="external" href="/design">Wedding design</a></li>
						<li><a class="external" href="/planning">Wedding planning</a></li>
						<li><a class=" external" href="/location">Location des matériels</a></li>

					</ul>

				</li>
				<li><a href="/media" class="external">Espace media</a></li>
				<li><a href="/actualites" class="external">Actualités</a></li>
				<li><a href="/contact" class="external">Nous contacter</a></li>
			</ul>
		</nav>
		<!-- /main nav -->

	</div>
</header>
<!-- End Fixed Navigation -->


<!--Service location -->

<section id="about" class="features">
	<div class="container">



		<div class="row">

			<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>Page non trouvée</h2>
				<div class="devider"><i class="far fa-heart"></i></i></div>
			</div>
			<div class=" text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>La page que vous cherchez n'existe pas </h2>
			</div>

		</div>

	</div>
</section>

<!-- End About ==================================== -->

<?php get_footer(); ?>