<?php get_header(); ?>



<header id="navigation" class="navbar-fixed-top navbar">
	<div class="container">
		<div class="navbar-header">
			<!-- responsive nav button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigation</span>
				<i class="fa fa-bars fa-2x"></i>
			</button>
			<!-- /responsive nav button -->

			<!-- logo -->
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
				<h1 id="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" width="50" alt="">
					<span class="logo-text">Orchidée Event</span>
				</h1>
			</a>
			<!-- /logo -->
		</div>

		<!-- main nav -->
		<nav class="collapse navbar-collapse navbar-right" role="navigation">
			<ul id="nav" class="nav navbar-nav  ">
				<li><a href="//orchideevent.net" class="external">Accueil</a></li>
				<li class="dropdown">
					<a class="external" href="" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Nos services<span class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown">
						<li><a class="external" href="/design">Wedding design</a></li>
						<li><a class="external" href="/planning">Wedding planning</a></li>
						<li><a class="external" href="/location">Location des matériels</a></li>

					</ul>

				</li>
				<li><a href="/media" class="external">Espace media</a></li>
				<li><a href="/actualites" class="external">Actualités</a></li>
				<li><a href="/contact" class="external current">Nous contacter</a></li>
			</ul>
		</nav>
		<!-- /main nav -->

	</div>
</header>

<section id="about" class="features padding-top-50">
	<div class="container">
		<div class="row">

			<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
				<h2>Ecrivez-nous pour toutes vos préoccupations</h2>
				<div class="devider"><i class="far fa-heart"></i></i></div>
			</div>
			<div class="sec-sub-title text-ceter wow rubberBand animated animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: rubberBand;">
				<p>Nous travaillons avec les meilleurs prestataires dans leur domaine et nous vous proposons des réalisations
					de qualité,
					sans surprise et aux meilleurs tarifs. Quel que soit votre budget, nous déclinons chaque concept de manière
					personnalisée afin de répondre au mieux à vos attentes.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="contact-form">
					<h3>Ecrivez-nous!</h3>
					<?php
					echo do_shortcode(
						'[contact-form-7 id="101" title="Page contact"]'
					);
					?>

				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div id="map_can" class="wow bounceInDown animated" data-wow-duration="500ms">
						<div class="gmap_canvas">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3978.2995372095725!2d15.261166214210355!3d-4.354806996834899!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1a6a312f260805b9%3A0x7d6cce09c0f8813d!2sorchid%C3%A9e%20event!5e0!3m2!1sfr!2scd!4v1574182032338!5m2!1sfr!2scd" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							<div class="lead">
								<div class="row">
									<div class="col-sm-6">
										<p>Orchidée Event</p>
										<p>07, chaussée de Benseke</p>
										<p>Ngaliema macampagne</p>
										<p><a style="color:#9DA4AA ;" href="tel:+243841200001">+243 84 12 00 001</a> </p>
										<p></p>
									</div>
									<div class="col-sm-6">
										<p>Suivez-nous</p>
										<ul class="social-links">
											<li><a style="color: #3B5998;" href="https://facebook.com/orchideevent/"><i class="fab fa-facebook-f fa-lg"></i></a></li>
											<li><a style="color: #D63086;" href="https://instagram.com/chezorchidee"><i class="fab fa-instagram fa-lg"></i></a></li>
											<li><a style="color: #0097FF;" href="https://m.me/orchideevent"><i class="fab fa-facebook-messenger fa-lg"></i></a></li>
											<li>
												<a style="color:#9DA4AA ;" href="mailto:infos@orchideevent.com">infos@orchideevent.com</a>
											</li>
										</ul>
									</div>
								</div>

							</div>

						</div>

					</div>
				</div>

			</div>


		</div>

	</div>
</section>

<?php get_footer(); ?>