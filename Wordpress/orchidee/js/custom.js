var wow = new WOW({
  boxClass: "wow", // animated element css class (default is wow)
  animateClass: "animated", // animation css class (default is animated)
  offset: 120, // distance to the element when triggering the animation (default is 0)
  mobile: false, // trigger animations on mobile devices (default is true)
  live: true // act on asynchronously loaded content (default is true)
});
wow.init();

$(document).ready(function() {
  $(window).scroll(function() {
    if ($(window).scrollTop() > 400) {
      $("#navigation,#dropdown").css("background-color", "#D29733");
    } else {
      $("#navigation,#dropdown").css(
        "background-color",
        "rgba(99, 64, 0, 0.6)"
      );
    }
  });

  /* ========================================================================= */
  /*	Fix Slider Height
	/* ========================================================================= */

  var slideHeight = $(window).height() / 1.3;

  $("#slider, .carousel.slide, .carousel-inner, .carousel-inner .item").css(
    "height",
    slideHeight
  );

  $(window).resize(function() {
    "use strict",
      $("#slider, .carousel.slide, .carousel-inner, .carousel-inner .item").css(
        "height",
        slideHeight
      );
  });

  /* ========================================================================= */
  /*	Dropdown menu on hover
/* ========================================================================= */

  $(function() {
    $(".dropdown").hover(
      function() {
        $(this).addClass("open");
      },
      function() {
        $(this).removeClass("open");
      }
    );
  });

  /* ========================================================================= */
  /*	Portfolio Filtering
	/* ========================================================================= */

  // portfolio filtering

  $(".project-wrapper").mixItUp();

  $(".fancybox").fancybox({
    padding: 0,

    openEffect: "elastic",
    openSpeed: 650,

    closeEffect: "elastic",
    closeSpeed: 550,

    closeClick: true
  });

  /* ========================================================================= */
  /*	Parallax
	/* ========================================================================= */

  $("#stats").parallax("50%", 0.3);

  /* ========================================================================= */
  /*	Timer count
	/* ========================================================================= */

  ("use strict");
  $(".number-counters").appear(function() {
    $(".number-counters [data-to]").each(function() {
      var e = $(this).attr("data-to");
      $(this)
        .delay(6e3)
        .countTo({
          from: 50,
          to: e,
          speed: 3e3,
          refreshInterval: 50
        });
    });
  });

  /* ========================================================================= */
  /*	Back to Top
	/* ========================================================================= */

  $(window).scroll(function() {
    if ($(window).scrollTop() > 400) {
      $("#back-top").fadeIn(200);
    } else {
      $("#back-top").fadeOut(200);
    }
  });
  $("#back-top").click(function() {
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: 0
        },
        1500,
        "easeInOutExpo"
      );
  });
});
